# Atlassian Ramp up Project (Stephen)

## Description

Codebasers Past and Present. Contact and other info displayed in card format.
![demo.gif](https://bitbucket.org/stephng3/atl-rampup-template-stephen/raw/c2729788b696388b986cf61c6a4f1c66bd55b504/demo.gif)


## Features

- `Tags` Codebasers are tagged according to their member status (active, social, alumni), graduation year, and past/present positions
- `Search` Select multiple tags to find the Codebasers that fit your criteria
- `Concurrency` Multiple rosters are downloaded concurrently with goroutines for lower latency


## Running the application

- Run ./start.sh

## AtlasKit Components Used

- [AvatarItem](https://atlaskit.atlassian.com/packages/core/avatar/docs/avatar-item)
- [Checkbox Select](https://atlaskit.atlassian.com/packages/core/select)
- [Icon](https://atlaskit.atlassian.com/packages/core/icon)
- [Page Header](https://atlaskit.atlassian.com/packages/core/page-header)
