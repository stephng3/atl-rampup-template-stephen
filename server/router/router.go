package router

import (
	"../models"
	"archive/zip"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gorilla/mux"
)


const (
	region = "us-west-1"
	bucket = "atlrampup"
)

var sess = session.Must(session.NewSession(&aws.Config{
	Region: aws.String(region),
}))
var svc = s3.New(sess)

// Router is exported and used in main.go
func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", GetCodebaseLogo).Methods(http.MethodGet)
	router.HandleFunc("/members", GetMembers).Methods(http.MethodGet)
	return router
}

// GetCodebaseLogo gets returns a json payload containing the URL for the club logo
func GetCodebaseLogo(w http.ResponseWriter, r *http.Request) {
	key := "codebaes.png"
	// Apparently getting an image URL is just constructing by hand lol
	output := models.Payload{
		ImageURL: fmt.Sprintf("https://s3-%s.amazonaws.com/%s/%s", region, bucket, key),
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(output)
}

func GetMembers(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query().Get("name")

	names := make([]string, 0)

	if len(params) < 1 {
		names = append(names, "active", "alumni", "social")
	} else {
		names = append(names, params)
	}

	members := make(chan []map[string]string)
	result := make([]map[string]string, 0)
	for _, n := range names {
		go func() { members <- GetRoster(n) }()
		re := <- members
		result = append(result, re...)
	}

	res, err := json.Marshal(result)
	if err != nil {
		log.Fatal(err)
	}

	_, _ = w.Write(res)
}

func GetRoster(name string) (result []map[string]string) {
	if name == "active" || name == "alumni" || name == "social" {
		roster := GetS3Object(fmt.Sprintf("%sroster.zip", name))
		var result = make([]map[string]string, 0)
		var headers = make([]string,0)
		for i, row := range roster {
			switch {
			case i == 0:
				for _, val := range row {
					// Weird bytes at the start of the csv
					val := string(bytes.Trim([]byte(val), "\xef\xbb\xbf"))
					if val == "First" {
						val = "Name"
					}
					if val == "Graduation Year" {
						val = "Class"
					}
					if val == "Roles Had" {
						val = "Role"
					}
					headers = append(headers, val)
				}
			case i > 0:
				m := make(map[string]string)
				for j, val := range row {
					m[headers[j]] = val
				}
				m["type"] = name
				result = append(result, m)
			}
		}
		return result
	} else {
		log.Fatal(fmt.Sprintf("Some error occured: %s", name))
	}
	return
}

func GetS3Object(item string) (result [][]string) {
	downloader := s3manager.NewDownloader(sess)
	buf, err := os.Create(item)

	numBytes, err := downloader.Download(buf,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(item),
		})
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to download item %q, %v", item, err)
	}
	r, err := zip.OpenReader(item)
	for _, f := range r.File {
		fmt.Printf("Processing %s...\n", f.Name)
		rc, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}
		r := csv.NewReader(rc)
		result, err = r.ReadAll()
		if err != nil {
			log.Fatal(err)
		}
		rc.Close()
	}

	fmt.Println("Downloaded", numBytes, "bytes")
	return
}
