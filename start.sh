#!/usr/bin/env sh

(go run server/main.go) & (cd frontend && yarn start)