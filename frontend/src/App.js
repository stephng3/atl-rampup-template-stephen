import React, {useEffect, useState} from "react"
import "./App.css"
import MemberCard from './MemberCard'
import {CheckboxSelect} from '@atlaskit/select'
import * as _ from 'lodash'
import PageHeader from '@atlaskit/page-header'

const cache = {}

function sortMembers(a, b) {
    const atc = b.aTags.length - a.aTags.length
    if (atc === 0) {
        for (let i = 0; i < atc; i++) {
            const tName = a.aTags[i].localeCompare(b.aTags[i])
            if (tName !== 0) {
                return tName
            }
        }
        return a.Name.localeCompare(b.Name)
    } else {
        return atc
    }
}

function App() {
    const [members, setMembers] = useState([])
    const [tags, setTags] = useState([])
    const [filteredMembers, setFilteredMembers] = useState([])
    const fetchMembers = async () => {
        await fetch("/members")
            .then(res => res.json())
            .then(m => {
                m = m.filter(mem => !!mem.Name && !!mem.Email)
                    .map((mem) => {
                    mem.dTags = [mem.type]
                    if (!!mem.Class) {
                        mem.dTags.push(mem.Class)
                    }
                    if (!!mem.Role) {
                        mem.dTags.push(...mem.Role.split(",").map(r => r.trim()).filter(r => !!r))
                    }
                    mem.aTags = []
                    return mem
                })
                const res = new Set()
                for (let mem of m) {
                    for (let tag of mem.dTags){
                        res.add(tag)
                    }
                }
                const tags = Array.from(res)
                    .filter(t => t)
                    .map(t => ({label: t[0].toUpperCase() + t.slice(1), value: t}))
                    .sort((a,b) => a.value.localeCompare(b.value))
                setMembers(m.sort(sortMembers))
                setFilteredMembers(_.cloneDeep(m))
                setTags(tags)
            })
    }
    useEffect(() => {fetchMembers()}, [])
    const filterMembers = (tags) => {
        if (!_.isEmpty(tags)) {
            if (!cache[tags]) {
                cache[tags] = _.cloneDeep(members)
                    .map(m => {
                        for (let t of tags) {
                            const idx = m.dTags.indexOf(t)
                            if (idx !== -1) {
                                const t = m.dTags.splice(idx, 1)
                                m.aTags.push(...t)
                            }
                        }
                        return m
                    })
                    .filter(m => m.aTags.length > 0)
                    .sort(sortMembers)
            }
            return cache[tags]
        }
        else {
            return members
        }
    }
    const onSelectTags = (tags) => {
        setFilteredMembers(filterMembers(tags ? tags.map(t => t.value) : []))
    }
  return (
      <div style={{padding: '1em 5em'}}>
          <PageHeader
              bottomBar={
                  <CheckboxSelect
                      className="checkbox-select"
                      classNamePrefix="select"
                      placeholder="Filter by tag"
                      options={tags}
                      onChange={onSelectTags}
                  />
              }>
              Codebase Past and Present
          </PageHeader>
          <div style={{display: 'flex', padding: '3em 6em'}}>
              <div style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                  justifyContent: 'center'
              }}>
                  {filteredMembers.map((m,i) => <MemberCard member={m} key={i}/>)}
              </div>
          </div>
      </div>
  )
}

export default App
