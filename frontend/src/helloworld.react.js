import React from "react";
import axios from "axios";

class HelloWorld extends React.Component {
  componentDidMount() {
    axios.get("/members?type=active").then(res => {
      if (res.data) {
        console.log(res.data);
      } else {
        console.log("No data found :(");
      }
    });
  }

  render() {
    return <div>Hello World!</div>;
  }
}

export default HelloWorld;
