import React from 'react'
import Avatar, { AvatarItem } from '@atlaskit/avatar';
import Lozenge from '@atlaskit/lozenge';
import { FaGithub,FaLinkedin, FaSnapchat, FaPhoneSquare } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";


const icons = {
    "Github": FaGithub,
    "LinkedIn": FaLinkedin,
    "Snapchat": FaSnapchat,
    "Instagram": AiFillInstagram,
    "Phone": FaPhoneSquare
}

const getLink = (domain, member) => {
    switch (domain) {
        case "Snapchat":
            return `https://www.snapchat.com/add/${member[domain]}`
        case "Instagram":
            return `https://www.instagram.com/${member[domain]}`
        case "Phone":
            return `tel:${member[domain]}`
        default:
            return member[domain]
    }
}

const PLozenge = ({ children, ...props}) => <span style={{margin: "0px 8px 0px 0px"}}><Lozenge {...props}>{children}</Lozenge></span>

const MemberCard = ({member: {Name, Email, aTags, dTags, ...others}}) => {
    return (
        <div className="member-card">
            <div style={{display:'flex', justifyContent: 'space-between'}}>
                <AvatarItem
                    avatar={<Avatar src={`https://api.adorable.io/avatars/285/${Email}`} />}
                    component={({children}) => (
                        <span
                            style={{
                                display: 'flex',
                                padding: '16px',
                                alignItems: 'center'
                            }}
                        >
                        {children}
                    </span>
                    )}
                    key={Email}
                    primaryText={Name}
                    secondaryText={Email}
                />
            </div>
            <div className="container" style={{minHeight: '35px'}}>
                {
                    Object.entries(icons)
                        .filter(([k,v]) => !!others[k])
                        .map(([k,v]) => (
                            <span
                                key={k}
                                style={{cursor: 'pointer'}}
                                onClick={() => window.open(getLink(k, others))}
                            >
                                {v({size: '2em', style: {padding: '0px 4px 0px 0px'}})}
                            </span>
                    ))
                }
            </div>
            <div className="container">
                {aTags.map((t,i) => <PLozenge key={i} appearance='success' isBold>{t}</PLozenge>)}
                {dTags.map((t,i) => <PLozenge key={i}>{t}</PLozenge>)}
            </div>
        </div>
    )
}

export default MemberCard